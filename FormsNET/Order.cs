﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace FormsFaluplast
{
    public class Order : IDisposable
    {
        Garp.Application GarpOrder = null;
        Garp.Dataset dsOGA, dsOGR;
        Garp.ITable tblAGA, tblAGS, tblOGR, tblOGM, tblKA;
        Garp.ITabField fld_OGR_OrderNr, fld_OGR_RadNr, fld_OGR_ArtNr, fld_OGR_Benamning, fld_OGR_Levtid, fld_OGR_DIM, fld_AGA_LagerTyp, fld_KA_KAT;
        Garp.IComponent LabelVer, LabelDevation, LabelEditDevation, LabelDevationText, LabelDividedCarton, Form_Order_Levtid, Form_OrderRad_LagerNr, Form_OrderRad_Antal, Form_OrderRad_Rabatt;

        string TmpOGRDIM, TmpOGALevtid, Order_Rad;

        string Version = "1.9";

        bool m_disposed = false;
        DialogResult Result { get; set; }

        Garp.IComponents ComponentsOrder;

        public Order()
        {
            GarpOrder = new Garp.Application();
            ComponentsOrder = GarpOrder.Components;

            // Hämta datakällan för OrderHuvud OrderRad
            dsOGA = GarpOrder.Datasets.Item("ogaMcDataSet");
            dsOGR = GarpOrder.Datasets.Item("ogrMcDataSet");

            // Skapa händelser
            dsOGA.AfterScroll += AfterScrollOGA;
            dsOGR.AfterScroll += AfterScrollOGR;
            dsOGR.BeforePost += BeforePostOGR;

            GarpOrder.FieldEnter += FieldEnter;
            GarpOrder.FieldExit += FieldExit;

            // Hämta tabeller
            tblAGA = GarpOrder.Tables.Item("AGA");
            tblAGS = GarpOrder.Tables.Item("AGS");
            tblOGR = GarpOrder.Tables.Item("OGR");
            tblOGM = GarpOrder.Tables.Item("OGM");
            tblKA = GarpOrder.Tables.Item("KA");

            // Tabellfält
            fld_OGR_OrderNr = tblOGR.Fields.Item("ONR");
            fld_OGR_RadNr = tblOGR.Fields.Item("RDC");
            fld_OGR_ArtNr = tblOGR.Fields.Item("ANR");
            fld_OGR_Benamning = tblOGR.Fields.Item("BEN");
            fld_OGR_Levtid = tblOGR.Fields.Item("LDT");
            fld_OGR_DIM = tblOGR.Fields.Item("DIM");

            fld_KA_KAT = tblKA.Fields.Item("KAT");

            fld_AGA_LagerTyp = tblAGA.Fields.Item("MLG");

            // Bevakning av fält
            Form_Order_Levtid = ComponentsOrder.Item("ogaBltMcEdit");

            Form_OrderRad_LagerNr = ComponentsOrder.Item("ogrLagMcEdit");
            Form_OrderRad_Antal = ComponentsOrder.Item("ogrOraMcEdit");
            Form_OrderRad_Rabatt = ComponentsOrder.Item("ogrRabMcEdit");

            SetUpOR();
        }

        private void SetUpOR()
        {
            try
            {
                ComponentsOrder.BaseComponent = "TabSheet3";

                // Visa versionsnummer
                if (LabelVer == null)
                {
                    LabelVer = ComponentsOrder.AddLabel("Ver");
                    LabelVer.Top = 300;
                    LabelVer.Left = 8;
                    LabelVer.Width = 40;
                    LabelVer.Text = "Faluplast forms version: " + Version;
                }

                // EDI-fält avvikelser
                if (LabelDevation == null)
                {
                    LabelDevation = ComponentsOrder.AddLabel("Devation");
                    LabelDevation.Top = 136;
                    LabelDevation.Left = 459;
                    LabelDevation.Width = 200;
                    LabelDevation.Text = "Status";
                    LabelDevation.Visible = false;

                    LabelEditDevation = ComponentsOrder.AddEdit("DevationNr");
                    LabelEditDevation.Top = 155;
                    LabelEditDevation.Left = 459;
                    LabelEditDevation.Width = 20;
                    LabelEditDevation.Height = 21;
                    LabelEditDevation.MaxLength = 1;
                    LabelEditDevation.TabStop = false;
                    LabelEditDevation.Visible = false;

                    LabelDevationText = ComponentsOrder.AddEdit("DevationText");
                    LabelDevationText.Top = 155;
                    LabelDevationText.Left = 483;
                    LabelDevationText.Width = 80;
                    LabelDevationText.Height = 21;
                    LabelDevationText.Color = 15592941;
                    LabelDevationText.ReadOnly = true;
                    LabelDevationText.TabStop = false;
                    LabelDevationText.Visible = false;
                }
                if (LabelDividedCarton == null)
                {
                    LabelDividedCarton = ComponentsOrder.AddLabel("LblDevidedCarton");
                    LabelDividedCarton.Top = 117;
                    LabelDividedCarton.Left = 459;
                    LabelDividedCarton.Color = 6740479;
                    LabelDividedCarton.Text = "Delad kartong!";
                    LabelDividedCarton.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Initiering OrderRad " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void UpdateMtrlPlan()
        {
            try
            {
                // Loopa materialraderna
                tblOGM.Find(Order_Rad);
                tblOGM.Next();
                while (tblOGM.Eof != true && tblOGM.Fields.Item("ONR").Value == dsOGA.Fields.Item("ONR").Value && tblOGM.Fields.Item("RDC").Value == dsOGR.Fields.Item("RDC").Value)
                {
                    // X-antal ej uppdaterat tidigare
                    if (double.Parse(tblOGM.Fields.Item("DIM").Value, CultureInfo.InvariantCulture) == 0)
                    {
                        string index, mtrlAmountString, structureAmountString;
                        double structureAmount, mtrlAmount;
                        int countDecimals = 0;

                        // Skapa index för sökning i struktur, Artikel från orderrad + ev. position + artikel från materialraden
                        string pos = tblOGM.Fields.Item("POS").Value;
                        if (pos == null)
                        { pos = "   "; }
                        index = Verktyg.fillBlankRight(dsOGR.Fields.Item("ANR").Value, 13) + Verktyg.fillBlankRight(pos, 3) + tblOGM.Fields.Item("ANR").Value;

                        // Hämta artikel från strukturen
                        if (tblAGS.Find(index))
                        {
                            // Strukturens antal x 1000 för att slippa decimalproblem
                            structureAmount = double.Parse(tblAGS.Fields.Item("KVE").Value.Replace(".", ",")) * 1000;

                            // Räkna ut antal decimaler från ingående antal i strukturen
                            countDecimals = CountDecimals(structureAmount);

                            // Spara materialradens antal
                            mtrlAmount = double.Parse(tblOGM.Fields.Item("ORA").Value, CultureInfo.InvariantCulture);

                            // Begränsa antalet decimaler för stora tal
                            countDecimals = LimitDecimals(mtrlAmount.ToString().Length, countDecimals);

                            // Formatera värden med rätt antal decimaler
                            mtrlAmountString = FormatDecimal(mtrlAmount, countDecimals);
                            structureAmountString = FormatDecimal(structureAmount, countDecimals);

                            // Uppdatera materialplaneringen
                            tblOGM.Fields.Item("DIM").Value = structureAmountString; // Uppdatera X-antal med strukturens antal per enhet
                            tblOGM.Fields.Item("ADE").Value = countDecimals.ToString(); // Uppdatera antalet decimaler
                            tblOGM.Fields.Item("ORA").Value = mtrlAmountString; // Uppdatera antalet med det uträknade antalet decimaler
                            tblOGM.Post();
                        }
                    }
                    tblOGM.Next();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Uppdatera materialplanering" + Environment.NewLine + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void EDIinfo()
        {
            try
            {
                // Kolla om kunden är EDI-kund. BYT DENNA SÖKNING TILL TEX KOD PÅ KUNDKATEGORI ELLER KOD PÅ KUND
                // Visa ev avvikelse på orderraden, fältet DIM

                if (tblKA.Find(dsOGA.Fields.Item("KNR").Value))
                {
                    switch (fld_KA_KAT.Value)
                    {
                        case "B1":
                        case "B2":
                        case "B3":
                            LabelDevation.Visible = true;
                            LabelEditDevation.Visible = true;
                            LabelDevationText.Visible = true;

                            LabelEditDevation.Text = Verktyg.Left(dsOGR.Fields.Item("DIM").Value, 1);

                            switch (dsOGR.Fields.Item("DIM").Value)
                            {
                                case "1":
                                case "1.00":
                                    LabelDevationText.Text = "Tillägg";
                                    break;
                                case "3":
                                case "3.00":
                                    LabelDevationText.Text = "Ändrad";
                                    break;
                                case "5":
                                case "5.00":
                                    LabelDevationText.Text = "Accepterad";
                                    break;
                                case "7":
                                case "7.00":
                                    LabelDevationText.Text = "Ej accepterad";
                                    break;
                                default:
                                    LabelEditDevation.Text = "";
                                    LabelDevationText.Text = "Info saknas";
                                    break;
                            }

                            break;

                        // släck fälten
                        default:
                            LabelDevation.Visible = false;
                            LabelEditDevation.Visible = false;
                            LabelDevationText.Visible = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Visa EDI-status " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void EDIRowUpdate()
        {
            try
            {
                // Kolla om DIM är ändrat
                if (LabelEditDevation != null && ComponentsOrder.CurrentField == LabelEditDevation.Name && TmpOGRDIM != LabelEditDevation.Text)
                {
                    if (tblOGR.Find(Verktyg.fillBlankRight(dsOGA.Fields.Item("ONR").Value, 6) + Verktyg.fillBlankLeft(dsOGR.Fields.Item("RDC").Value, 3)))
                    {
                        fld_OGR_DIM.Value = LabelEditDevation.Text;
                        tblOGR.Post();
                        SendKeys.SendWait("{F5}");
                    }
                }

                // Test om ny orderrad på EDI-kund, sätt DIM till 1
                // TODO: BYT DENNA SÖKNING TILL TEX KOD PÅ KUNDKATEGORI ELLER KOD PÅ KUND
                if (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name)
                {
                    if (
                        dsOGR.Fields.Item("ORA").Value != "0"
                        && tblKA.Find(dsOGA.Fields.Item("KNR").Value)
                        && (fld_KA_KAT.Value == "B1" || fld_KA_KAT.Value == "B2" || fld_KA_KAT.Value == "B3")
                        && tblOGR.Find(Order_Rad)
                        && (fld_OGR_DIM.Value == "0")
                        )
                    {
                        fld_OGR_DIM.Value = "1";
                        tblOGR.Post();
                    }
                }

                // Kolla om Orderhuvud levtid är ändrat
                // TODO: BYT DENNA SÖKNING TILL TEX KOD PÅ KUNDKATEGORI ELLER KOD PÅ KUND
                if (
                    ComponentsOrder.CurrentField == Form_Order_Levtid.Name
                    && TmpOGALevtid != dsOGA.Fields.Item("BLT").Value
                    && tblKA.Find(dsOGA.Fields.Item("KNR").Value)
                    && (fld_KA_KAT.Value == "B1" || fld_KA_KAT.Value == "B2" || fld_KA_KAT.Value == "B3")
                    )
                {
                    string text = string.Empty;
                    tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                    tblOGR.Next();
                    while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == dsOGA.Fields.Item("ONR").Value)
                    {
                        if (
                            fld_OGR_Levtid.Value == dsOGA.Fields.Item("BLT").Value
                            && (fld_OGR_DIM.Value == "" || fld_OGR_DIM.Value == "0" || fld_OGR_DIM.Value == "5")
                            )
                        {
                            text = text + fld_OGR_RadNr.Value + " , " + fld_OGR_ArtNr.Value + " , " + fld_OGR_Benamning.Value + Environment.NewLine;

                            // Ändra DIM på raderna till 3
                            fld_OGR_DIM.Value = "3";
                            tblOGR.Post();
                        }
                        tblOGR.Next();
                    }

                    if (text != string.Empty)
                    {
                        MessageBox.Show("Leveranstiden ändrad!" + Environment.NewLine + "Avvikelsekoden ändrad på dessa rader:" + Environment.NewLine + text, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EDIRowUpdate" + Environment.NewLine + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void CheckStockNumber()
        {
            try
            {
                if (tblAGA.Find(dsOGR.Fields.Item("ANR").Value) && fld_AGA_LagerTyp.Value != null && dsOGR.Fields.Item("LAG").Value == null)
                {
                    MessageBox.Show("Lagernummer saknas!", "Garp", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Form_OrderRad_LagerNr.SetFocus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Test om lagernummer är ifyll" + Environment.NewLine + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void InternalOrder()
        {
            try
            {
                // Sätt OB och PL flaggor till 1 på ny internorder
                if (dsOGA.Fields.Item("OTY").Value == "9")
                {
                    tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                    tblOGR.Next();
                    if (fld_OGR_OrderNr.Value != dsOGA.Fields.Item("ONR").Value)
                    {
                        dsOGA.Fields.Item("OBF").Value = "1";
                        dsOGA.Fields.Item("PLF").Value = "1";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Uppdatera internorder" + Environment.NewLine + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void OHRab()
        {
            try
            {
                // Ingen uppdatering på kostnadsrader
                if (Int32.Parse(dsOGR.Fields.Item("RDC").Value) < 251)
                {
                    // Test om orderhuvudrabatt
                    if (double.Parse(dsOGR.Fields.Item("RAB").Value, CultureInfo.InvariantCulture) != 0)
                    {
                        dsOGR.Fields.Item("HRF").Value = "N";
                    }
                    else
                    {
                        dsOGR.Fields.Item("HRF").Value = "";
                    }
                    SendKeys.SendWait("{F5}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Test orderhuvudrabatt" + Environment.NewLine + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DividedCarton()
        {
            try
            {
                // Test om artikeln har mer än 1 i antal/enhet
                if (tblAGA.Find(dsOGR.Fields.Item("ANR").Value) && int.Parse(tblAGA.Fields.Item("APE").Value) > 1)
                {
                    if (double.Parse(dsOGR.Fields.Item("ORA").Value) % int.Parse(tblAGA.Fields.Item("APE").Value) == 0)
                    { LabelDividedCarton.Visible = false; }
                    else
                    { LabelDividedCarton.Visible = true; }
                }
                else
                { LabelDividedCarton.Visible = false; }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Test om antal är delbart med antal/enh" + Environment.NewLine + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollOGA()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGA " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollOGR()
        {
            try
            {
                if (dsOGR.Fields.Item("RDC").Value != null)
                {
                    if (dsOGA.Fields.Item("ONR").Value != null)
                    {
                        Order_Rad = Verktyg.fillBlankRight(dsOGA.Fields.Item("ONR").Value, 6) + Verktyg.fillBlankLeft(dsOGR.Fields.Item("RDC").Value, 3);
                    }

                    // Info om EDI-status
                    EDIinfo();

                    // Test om antal är delbart med antal/enh
                    if (double.Parse(dsOGR.Fields.Item("ORA").Value, CultureInfo.InvariantCulture) != 0)
                    { DividedCarton(); }
                    else
                    { LabelDividedCarton.Visible = false; }

                    // Uppdatera materialplaneringen med x-antal från strukturen
                    if (dsOGR.Fields.Item("ORA").Value != "0" && dsOGR.Fields.Item("MTF").Value == "1")
                    {
                        UpdateMtrlPlan();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGR " + ex + Environment.NewLine + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldEnter()
        {
            try
            {
                InternalOrder();

                // Spara värdet från fältet Orderhuvud levtid
                if (ComponentsOrder.CurrentField == Form_Order_Levtid.Name)
                {
                    TmpOGALevtid = dsOGA.Fields.Item("BLT").Value;
                }
                // Spara värdet från fältet DIM
                if (LabelEditDevation != null && ComponentsOrder.CurrentField == LabelEditDevation.Name)
                {
                    TmpOGRDIM = LabelEditDevation.Text;
                }

                //GarpApp.Components.Item(GarpApp.Components.CurrentField).SetFocus()
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldEnter " + ex, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldExit()
        {
            try
            {
                // Kolla att lagernummer är ifyllt
                if (ComponentsOrder.CurrentField == Form_OrderRad_LagerNr.Name)
                {
                    CheckStockNumber();
                }

                // Test om antalsfältet är ifyllt
                bool TestQty = false;
                if (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name && double.Parse(dsOGR.Fields.Item("ORA").Value, CultureInfo.InvariantCulture) != 0)
                { TestQty = true; }
                else
                { TestQty = false; }

                // Test om antal är delbart med antal/enh
                if (TestQty)
                {
                    DividedCarton();
                }

                // Test om orderhuvudrabatt
                if (TestQty || ComponentsOrder.CurrentField == Form_OrderRad_Rabatt.Name)
                {
                    OHRab();
                }

                // Uppdatera värden för EDI-kund
                EDIRowUpdate();

            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit " + ex, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePostOGR()
        {
            try
            {
                Order_Rad = Verktyg.fillBlankRight(dsOGA.Fields.Item("ONR").Value, 6) + Verktyg.fillBlankLeft(dsOGR.Fields.Item("RDC").Value, 3);

                // Testa om fältet lagernummer är ifyllt och antal <> 0
                if (tblOGR.Find(Order_Rad) && dsOGR.Fields.Item("ORA").Value != "0")
                {
                    CheckStockNumber();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("BeforePost " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private string FormatDecimal(double value, int decimals)
        {
            try
            {
                // Formatera en sträng med rätt antal decimaler
                string formatWithDecimal, amount;
                formatWithDecimal = String.Concat("{0:F", decimals, "}");
                amount = String.Format(formatWithDecimal, value).Replace(",", ".");
                if (amount.Length > 10)
                { amount = Verktyg.Left(amount, 10); }

                return amount;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Decimalformat" + Environment.NewLine + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return "0";
            }
        }

        private int CountDecimals(double value)
        {
            try
            {
                // Räkna ut antal decimaler från värde
                int Decimals = 0;
                decimal testDecimals = Convert.ToDecimal(value);
                testDecimals = Math.Abs(testDecimals);
                testDecimals -= (int)testDecimals;
                while (testDecimals > 0)
                {
                    Decimals++;
                    testDecimals *= 10;
                    testDecimals -= (int)testDecimals;
                }
                return Decimals;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Decimalformat" + Environment.NewLine + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return 0;
            }
        }

        private int LimitDecimals(int lenght, int decimals)
        {
            try
            {
                if (lenght == 9)
                { decimals = 0; }
                if (lenght == 8 && decimals > 1)
                { decimals = 1; }
                return decimals;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Begränsa antal decimaler" + Environment.NewLine + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return 0;
            }
        }

        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected virtual void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        if (GarpOrder != null)
                        {
                            GarpOrder.FieldExit -= FieldExit;
                            GarpOrder.FieldEnter -= FieldEnter;
                            dsOGR.BeforePost -= BeforePostOGR;
                            dsOGR.AfterScroll -= AfterScrollOGR;
                            dsOGA.AfterScroll -= AfterScrollOGA;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpOrder);
                        }
                    }
                }
                m_disposed = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Order()
        {
            Dispose(false);
        }
    }
}

