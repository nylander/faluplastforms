﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsFaluplast
{
    public class Verktyg
    {
        /// <summary>
        /// Antal tecken från vänster
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från vänster</param>
        /// <returns></returns>
        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        /// Antal tecken från höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från höger</param>
        /// <returns></returns>
        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// Tecken i mitten
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <param name="length">Antal tecken</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        /// Från mitten till höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till höger
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankRight(string param, int length)
        {
            string result = param.PadRight(length, ' ');
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till vänster
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankLeft(string param, int length)
        {
            string result = param.PadLeft(length, ' ');
            return result;
        }

    }
}
