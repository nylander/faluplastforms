﻿Tabellregistret: W200FB000
Text2: FormsFaluplast.Order
Kod 1: T

Kopiera och registrera filen FormsFaluplast.dll till en lokal mapp hos klienten

Registrera med Kommandotolken som körs som administratör.

C:\Windows\Microsoft.NET\Framework\v4.0.30319\regasm /codebase "c:\Temp\FormsFaluplast.dll""

Avregistrera:
C:\Windows\Microsoft.NET\Framework\v4.0.30319\regasm /u "c:\Temp\FormsFaluplast.dll"

Version 1.9
Ingen Orderhuvudrabatt på kostnadsrader

Version 1.8
Varning om antal på orderraden är ej jämnt delbart med artikelns antal/ enh
Om orderraden har en rabatt tas orderhuvudets rabatt bort

Version 1.7
Städat i koden
När materialrad planeras från orderraden läggs antalet från artikelns struktur till materialradens X-antal. Antalet från strukturen multipliceras med 1000.

Version 1.6
Klarknappen visas ej på internorder

Version 1.5
Rättat bugg för fel när man lämnar fältet avvikelsekoder

Version 1.4
Rättat bugg vid inläsning order-rad

Version 1.3
När man ändrar leveranstid på orderhuvud ändras avvikelsekoden (DIM) till 3 = ändrad på de rader som får ny leveranstid och avvikelsekoden är blank eller 5 = accepterad.
Nya orderrader får avvikelsekoden 1 = tillägg.

Version 1.2
Rättning visningen av DIM-fältet

Version 1.1
Fältet DIM (extra antal visas och är editerbart), används till EDI-kunder för eventuella avvikelsekoder
Version 1.0
Bevakning av fältet Lagernummer om det är ifyllt för flerlagerartikel.

